package utils;
import testlink.api.java.client.TestLinkAPIClient;
import testlink.api.java.client.TestLinkAPIException;

public class TestLinkUtils {
    // Replace Testlink API Interface access key here
    public final static String ACCESS_KEY = "c16fe16fcd17cfff85adb1d8b53659c5";
    //public final static String ACCESS_KEY = "0041b11e60b83e3a6ea42d74a669498a";
    
    // Replace your TestLink Server URL here
    public final static String TESTLINK_SERVER_URL = "http://lesse.com.br/testlink/lib/api/xmlrpc/v1/xmlrpc.php";
 
    // Replace Project Name here
    public final static String TESTLINK_PROJECT_NAME = "Grupo3";
 
    // Replace your Test Plan here
    public final static String TESTLINK_TESTPLAN_NAME = "GerenciadorPampatec";
 
    // Replace your Build Name here
    public final static String BUILD_RELEASE_NAME = "Autenticacao";
    
    public static void reportResult(String testCaseID, String executionNotes, String result) throws TestLinkAPIException {
        TestLinkAPIClient apiClient = new TestLinkAPIClient(ACCESS_KEY,TESTLINK_SERVER_URL);
        apiClient.reportTestCaseResult(TESTLINK_PROJECT_NAME, TESTLINK_TESTPLAN_NAME,testCaseID, BUILD_RELEASE_NAME, executionNotes, result);
   }
}